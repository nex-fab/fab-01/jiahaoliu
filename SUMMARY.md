# Summary

## Tutorials
* [1.Project manage]()
    * [Assessment](doc/1projectmanage/Assessment1project-manage.md)
    * [PRACTIVE](liuliuliu/1pm/pracvtive.md)  
* [2. CAD design]() 
    * [PRACTIVE2](liuliuliu/2cad/3.28practive.md)
* [3. 3D printer]()
    * [PRACTIVE3](liuliuliu/3.3D-printer/4.3practive.md)
    * [FAQ](liuliuliu/3.3D-printer/4.21-questiom.md)
* [4. Electric design]()
    * [PRACTIVE4](liuliuliu/4.Electric-design/cadence.md)
* [5. Arduino]()
    * [PRACTIVE5](liuliuliu/5.Arduino/arduino.md)
* [6.Laser cut]()
    * [PRACTIVE6](liuliuliu/6.laser-cut/laser-cutting.md)
    * [FAQ](liuliuliu/6.laser-cut/FAQ.MD)
    * [CLASS](liuliuliu/6.laser-cut/Introduction.md)
    * [PIC TO CAD](liuliuliu/6.laser-cut/pic-cad.md)

