#  LASER CUTTING
  
![](https://gitlab.com/picbed/bed/uploads/544c5974ab0d136f2ea1d8b958295072/laser-cut.gif )
  
  
![](https://gitlab.com/picbed/bed/uploads/967a90f13442f1ee37ed3c96044aff8e/powercut.png )
![](https://gitlab.com/picbed/bed/uploads/967a90f13442f1ee37ed3c96044aff8e/powercut.png )
  
![](https://gitlab.com/picbed/bed/uploads/7c83e2d3254ba5262d3c7ba00721dcee/part.png )
![](https://gitlab.com/picbed/bed/uploads/6d2b0b375df7297172fbb867a584484b/WechatIMG84.jpeg )
![](https://gitlab.com/picbed/bed/uploads/9588cdde5ac47cc7a32ae2932e5160bb/WechatIMG86.jpeg )
![](https://gitlab.com/picbed/bed/uploads/3cc5e2b0029bde30e41230b35a7aceb8/focus_point.png )
![](https://gitlab.com/picbed/bed/uploads/d8ef4b622e52293dd634a354739c50a5/machine_running.png )
  
###  HOMEWORK(traffic light)
  
####  ————the part of 2D/3D/laser cutting.
  
  
![](https://gitlab.com/pic-01/pic-liu/uploads/b2d3430e3be26d559ef7622b3f4c15fd/20200508172558.png )
  
![](https://gitlab.com/pic-01/pic-liu/uploads/731dfd2a429d8218e4407993f6f3f3d9/1588930141_1_.png )
  
![](https://gitlab.com/pic-01/pic-liu/uploads/a48118567c7b1a394cc522afc18207c4/20200511170218.png )
  
#### <center>The cutting situation of 5mm acrylic sheet </center>
<style>
table {
margin: auto;
}
</style>

| speed\power   | 10 | 20 | 30 | 40 | 50 | 60 | 70 | 80 |
| ------------- |:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| 10            |  N |  / |  / |  / |  / |  / |  / |  Y |
| 20            |  N |  N |  N |  N |  N |  N |  N |  Y |
| 30            |  N |  N |  N |  N |  N |  N |  N |  N |


![](https://gitlab.com/pic-01/pic-liu/uploads/a3841dcf1fb4b478dd81120a2acd2d0f/f5e59d1b5f9715c45da6216110cfcba.jpg)
  
![](https://gitlab.com/pic-01/pic-liu/uploads/6f38aaffe2a49623173eee03c3db9075/daac0cc6c1d5fa931dc4acfbd154d6f.jpg )
  
![](https://gitlab.com/pic-01/pic-liu/uploads/f8816f1b663b3cdcafd7d618e1cbf53e/8a51dbd840853b51a1a9fa51818606e.jpg )
  
![](https://gitlab.com/pic-01/pic-liu/uploads/c404df2f4e56f96a5b9c4f8cee8f53a7/b469cf0a9593f9cd65bd669fb1eff94.jpg )
  
![](https://gitlab.com/pic-01/pic-liu/uploads/1e64a0edf0314d40e3af14f5d16a4333/a6578526b79746a99b7bd7d36a51d85.jpg )
  
![](https://gitlab.com/pic-01/pic-liu/uploads/ada8cb2e4c9653e2709c31d07fc49b01/eed731ce14cbe734de7e06f99d06279.jpg )
  
![](https://gitlab.com/pic-01/pic-liu/uploads/9e9e58444e186dedf68aea29d88b578d/3.gif )
  
  