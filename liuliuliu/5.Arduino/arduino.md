# Arduino

![](https://gitlab.com/picbed/bed/uploads/9d4306ae40072cc40857177a01d4f73c/a000066_featured_5.jpg)

### Learning process
#### 1.OUTPUT LCD displays
![](https://gitlab.com/pic-01/pic-liu/uploads/0f2ccac2ed42df36e60406315868a947/8a19288de14fdc678878e2e096a2734.jpg)
![](https://gitlab.com/picbed/bed/uploads/b3403ee8256dd4ce28c61e31719b3377/LCD_Base_bb_Fritz.png)

```
  LiquidCrystal Library - Hello World

 Demonstrates the use a 16x2 LCD display.  The LiquidCrystal
 library works with all LCD displays that are compatible with the
 Hitachi HD44780 driver. There are many of them out there, and you
 can usually tell them by the 16-pin interface.

 This sketch prints "Hello World!" to the LCD
 and shows the time.

  The circuit:
 * LCD RS pin to digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * LCD VSS pin to ground
 * LCD VCC pin to 5V
 * 10K resistor:
 * ends to +5V and ground
 * wiper to LCD VO pin (pin 3)

 Library originally added 18 Apr 2008
 by David A. Mellis
 library modified 5 Jul 2009
 by Limor Fried (http://www.ladyada.net)
 example added 9 Jul 2009
 by Tom Igoe
 modified 22 Nov 2010
 by Tom Igoe

 This example code is in the public domain.

 http://www.arduino.cc/en/Tutorial/LiquidCrystal
 */

// include the library code:
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("hello, world!");
}

void loop() {
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(0, 1);
  // print the number of seconds since reset:
  lcd.print(millis() / 1000);
}
```
####  2.Button control
![](https://gitlab.com/pic-01/pic-liu/uploads/40c63e1a4fb835669bc8164105fc0056/1.gif)
#### 3.Potentiometer control
![](https://gitlab.com/pic-01/pic-liu/uploads/2eb44f1a1b31c69b01715bcad2388ee2/2.gif)

#### FAQ
##### ————The question can't be solved for the moment.
![](https://gitlab.com/pic-01/pic-liu/uploads/0ecfddb5281f7f683f0e0ab957ac606f/506163a4b48d8e043ba62b020c6ddd7.jpg)

### HOMEWORK(traffic light)
#### ————the prat of Arduino
```
//为各个输出端起别名
int  Led0 = 2; //Red1
int  Led1 = 3; //Green1
int  Led2 = 4; //Yellow1
int  Led3 = 5; //Red2
int  Led4 = 6; //Green2
int  Led5 = 7; //Yellow2
int  button = 8;
void setup() {
  unsigned char i;
  for (i = 2; i < 9; i++)
  pinMode(i, OUTPUT);  //循环设置Ledi为输出
  digitalWrite(i, LOW);   //熄灭Ledi
  pinMode(button,INPUT);
}
void Styl1(void) 
{
  unsigned char i;
  for (i = 0; i < 20; i++)
  {
    digitalWrite(Led5, LOW);    // 熄灭LED5(Yellow2)
    digitalWrite(Led0, LOW);    // 熄灭LED0(Red1)
    digitalWrite(Led1, HIGH);   //点亮LED1(Green1)
    digitalWrite(Led3, HIGH);   //点亮LED3(Red2)
    delay(500);    //延时10秒
  
  if(digitalRead(button)==HIGH)
  {
    digitalWrite(Led1,LOW);//CL
    unsigned char n;
    for (n = 0; n < 5; n++)
    {
      digitalWrite(Led2,HIGH);
      delay(500);
      digitalWrite(Led2,LOW);
      delay(500);
      
    }
    digitalWrite(Led0,HIGH);
    digitalWrite(Led4,HIGH);
    digitalWrite(Led3,LOW);
    delay(10000);
    digitalWrite(Led4,LOW);
    Styl5();
    Styl6();
  }
  
  }
}
void Styl2(void) 
{
  unsigned char i;
  for (i = 0; i < 4; i++) 
  {
    digitalWrite(Led1, LOW);    // 熄灭LED1(Green1)
    delay(500);              //等待500毫秒
    digitalWrite(Led1, HIGH);   //点亮LED1(Green1)
    delay(500);              //等待500毫秒
  }
}
void Styl3(void) {
  digitalWrite(Led1, LOW);    // 熄灭LED1(Green1)
  digitalWrite(Led2, HIGH);   //点亮LED2(Yellow1)
  delay(5000);    //延时5秒
}
void Styl4(void) {
  digitalWrite(Led2, LOW);    // 熄灭LED2(Yellow1)
  digitalWrite(Led3, LOW);    // 熄灭LED3(Red2)
  digitalWrite(Led0, HIGH);   //点亮LED0(Red1)
  digitalWrite(Led4, HIGH);   //点亮LED4(Green2)
  delay(10000);    //延时10秒
}
void Styl5(void) {
  unsigned char j;
  for (j = 0; j < 4; j++) {
    digitalWrite(Led4, LOW);    // 熄灭LED4(Green2)
    delay(500);              //等待500毫秒
    digitalWrite(Led4, HIGH);   //点亮LED4(Green2)
    delay(500);            //等待500毫秒
  }
}
void Styl6(void) {
  digitalWrite(Led4, LOW);    // 熄灭LED4(Green2)
  digitalWrite(Led5, HIGH);   //点亮LED5(Yellow2)
  delay(5000);    //延时5秒
}

void loop() {
  Styl1();
  Styl2();
  Styl3();
  Styl4();
  Styl5();
  Styl6();
}
```